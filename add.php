<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <meta name="author" content="Deniss Moisejs">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">  
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>Product List</title>
  </head>
  <body>
      
     <div class="container">
         
         <form action="#" method="post">
         <nav class="navbar navbar-expand-lg navbar-light">
          <div class="container-fluid">
            <div class="navbar-brand">Product Add</div>
            <div class="collapse navbar-collapse">
              <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              </ul>
                <button class="btn btn-light btn-add" type="submit" name="submit">Save</button>
                <a href="list.php"><div class="btn btn-light">Cancel</div></a>

            </div>
          </div>
        </nav>
        <hr class="top">
        <div class="container">
            
               <div class="row align-items-center">
                  <div class="col-2">
                    <label for="skm" class="col-form-label">SKU</label>
                  </div>
                  <div class="col-3">
                    <input type="text" id="skr" class="form-control" name="skr" required="">
                  </div>
              </div>
            
              <br>
            
              <div class="row align-items-center">
                  <div class="col-2">
                    <label for="name" class="col-form-label">Name</label>
                  </div>
                  <div class="col-3">
                    <input type="text" id="name" class="form-control" name="name" required="">
                  </div> 
              </div>
            
              <br>
            
              <div class="row align-items-center">
                  <div class="col-2">
                    <label for="price" class="col-form-label">Price ($)</label>
                  </div>
                  <div class="col-3">
                    <input type="number" step="0.01" min="0" id="price" class="form-control" name="price" required="">
                  </div> 
              </div>
            
              <br>
            
              <div class="row align-items-center">
                  <div class="col-2">
                    <label for="price" class="col-form-label">Type Switcher</label>
                  </div>
                  <div class="col-2">
                    <select class="form-select" id="type-selector" name="type-selector" required="">
                      <option value="" selected>Open this menu</option>
                      <option value="1">DVD</option>
                      <option value="2">Furniture</option>
                      <option value="3">Book</option>
                    </select>
                  </div> 
              </div>
            
             <div id="type-div">
            
             </div>

        </div>
         </form>  
       <hr class="bottom">
         <div class="scandiweb">Scandiweb Test assignment</div>
     </div> 
  
  <?php
      
        if(isset($_POST['submit'])){
                  
                  require_once 'controllers/product.php';
              
                  $newproduct = new Product();
                  $newproduct->add($_POST['skr'], $_POST['name'], $_POST['price'], $_POST['type-selector'], $_POST['size'] | 0, $_POST['height'] | 0, $_POST['width'] | 0, $_POST['lenght'] | 0, $_POST['weight'] | 0);
            
                  header("Location: list.php");
                  die();
      }
      
  ?>
 
  <script type="text/javascript">
      let selectObj = document.getElementById('type-selector');
      let typeObj = document.getElementById('type-div');
      //console.log(selectObj.value);
      
      selectObj.onchange = function add_inputs() {
          //console.log(selectObj.value);
          if(selectObj.value == 1) {
          typeObj.innerHTML = "<br><div class='row align-items-center'><div class='col-2'><label for='size' class='col-form-label'>Size (MB)</label></div><div class='col-3'><input type='number' min='0' id='size' class='form-control' name='size' required=''></div> </div><br><p>Please provide the size of the disc space in megabytes</p>";
          } else if(selectObj.value == 2) {
              typeObj.innerHTML = "<br><div class='row align-items-center'><div class='col-2'><label for='height' class='col-form-label'>Height (CM)</label></div><div class='col-3'><input type='number' min='0' id='height' class='form-control' name='height' required=''></div></div><br><div class='row align-items-center'><div class='col-2'><label for='height' class='col-form-label'>Width (CM)</label></div><div class='col-3'><input type='number' min='0' id='width' class='form-control' name='width' required=''></div></div><br><div class='row align-items-center'><div class='col-2'><label for='lenght' class='col-form-label'>Lenght (CM)</label></div><div class='col-3'><input type='number' min='0' id='lenght' class='form-control' name='lenght' required=''></div></div><br><p>Please provide dimensions in HxWxL</p>";
          } else if(selectObj.value == 3) {
              typeObj.innerHTML = "<br><div class='row align-items-center'><div class='col-2'><label for='weight' class='col-form-label'>Weight (KG)</label></div><div class='col-3'><input type='number' min='0' id='weight' class='form-control' name='weight' required=''></div></div><br><p>Please provide a weight in kilograms</p>";
          }
      }   
  </script>
      
  </body>
</html>