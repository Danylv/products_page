
<?php

require_once "product.php";
  
      class Dvd extends Product implements ProductTemplate
      {
              
          protected function getData() {
              
              global $conn;
              
              $sql = "SELECT id, skr, name, price, type, size FROM products WHERE type = 1 ORDER BY id";
              $result = $conn->query($sql);
 
              while($row = $result->fetch_assoc()) {
                  
                  $values[] = ['id' => $row['id'],
                                'skr' => $row['skr'],
                                'name' => $row['name'],
                                'price' => $row['price'],
                                'type' => $row['type'],
                                'size' => $row['size']
                               ];
                           
                             
            }
              return $this->values = $values;
        }
          
        protected function speacialInfo($item) {
              echo "Size: ".$item['size']." MB";
          }
          
      };

                 
?>