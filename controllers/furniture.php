
<?php
           
require_once "product.php";

      class Furniture extends Product implements ProductTemplate
      {
                 
          protected function getData() {
              
              global $conn;
              
              $sql = "SELECT id, skr, name, price, type, height, width, length FROM products WHERE type = 2 ORDER BY id";
              $result = $conn->query($sql);
              while($row = $result->fetch_assoc()) {
                  
                  $values[] = ['id' => $row['id'],
                                'skr' => $row['skr'],
                                'name' => $row['name'],
                                'price' => $row['price'],
                                'type' => $row['type'],
                                'height' => $row['height'],
                                'width' => $row['width'],
                                'length' => $row['length']
                               ];
              }
              return $this->values = $values;
          }
          
          protected function speacialInfo($item) {
              echo "Dimension: ".$item['height']."x".$item['width']."x".$item['length'];
          }
          
      };

                 
?>