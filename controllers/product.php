
<?php
           
require_once "db_connect.php";

      interface ProductTemplate
      {
          public function __construct();
          public function show();
          
      }


      class Product implements ProductTemplate
      {
          public $values;
          
          public function __construct() {
              $this->getData();
          }
          
          protected function getData() {}
          
          protected function speacialInfo($item) {}
          
          public function add($skr, $name, $price, $type, $size = 0, $height = 0, $width = 0, $length = 0, $weight = 0) {
              
              global $conn; 
              
              $sql = "INSERT INTO products (skr, name, price, type, size, height, width, length, weight)
              VALUES ('".$skr."', '".$name."', '".$price."', '".$type."', '".$size."', '".$height."', '".$width."', '".$length."', '".$weight."')";

                  if ($conn->query($sql) === TRUE) {
                      //echo "<p style='color: #06E003'><b>New product added successfully</b></p>";
                    } else {
                      echo "Error: " . $sql . "<br>" . $conn->error;
                    } 
              
          }
          
          
          public function show() {
              
              foreach ($this->values as $item) {
                  echo "<div class='col-3'><div class='card' style='width: 15rem;'><div class='card-body'><input class='form-check-input' type='checkbox' value='".$item['id']."' name='check_list[]' id='flexCheckDefault'><div class='product-info'><div>";
                  echo strtoupper($item['skr']);
                  echo "</div><div>";
                  echo $item['name'];
                  echo "</div><div>";
                  echo "Price: ".$item['price']." $";
                  echo "</div><div>";                               
                  $this->speacialInfo($item);                 
                  echo "</div></div></div></div></div>";
                  
              }
          }
          
          
          public function delete($post) {
              
              global $conn;
              
              if(!empty($post)){
                foreach($_POST['check_list'] as $selected){
                $sql = "DELETE FROM products WHERE id='".$selected."'";
                          if ($conn->query($sql) === TRUE) {
                              //echo "<p><b>DELETED SUCCESFULLY!</b></p>";
                            } else {
                              echo "Error: " . $sql . "<br>" . $conn->error;
                            }
                    }
                } else {
                    //echo "<b>NO SELECTED</b>";
                }

            }
          
      };

                 
?>