
<?php
           
require_once "product.php";

      class Book extends Product implements ProductTemplate
      {
                 
          protected function getData() {
            
              global $conn;
              
              $sql = "SELECT id, skr, name, price, type, weight FROM products WHERE type = 3 ORDER BY id";
              $result = $conn->query($sql);
              while($row = $result->fetch_assoc()) {
                  
                  $values[] = ['id' => $row['id'],
                                'skr' => $row['skr'],
                                'name' => $row['name'],
                                'price' => $row['price'],
                                'type' => $row['type'],
                                'weight' => $row['weight']
                               ];
                  
              }
              return $this->values = $values; 
          }
          
          
        protected function speacialInfo($item) {
              echo "Weight: ".$item['weight']." KG";
          }
          
      };

                 
?>