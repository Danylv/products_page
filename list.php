<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <meta name="author" content="Deniss Moisejs">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">  
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>Product List</title>
  </head>
  <body>
      
     <div class="container">
         
         <form action="#" method="post">
         <nav class="navbar navbar-expand-lg navbar-light">
          <div class="container-fluid">
            <div class="navbar-brand">Product List</div>
            <div class="collapse navbar-collapse">
              <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              </ul>
                <a href="add.php"><div class="btn btn-light btn-add">ADD</div></a>
                <button class="btn btn-light" type="submit" name="submit">MASS DELETE</button>

            </div>
          </div>
        </nav>
        <hr class="top">
        <div class="container">
          <div class="row">

           
              <?php
              
              require_once 'controllers/dvd.php';
              require_once 'controllers/furniture.php';
              require_once 'controllers/book.php';
              
              $dvd = new Dvd();
              $furniture = new Furniture();
              $book = new Book();
              
              $product_list = [$dvd, $furniture, $book];
              
              foreach($product_list as $product) {
                  $product->show();
              }
                                        
              ?>
              
              
          </div>
        </div>
         </form>  
       <hr class="bottom">
         <div class="scandiweb">Scandiweb Test assignment</div>
     </div> 
      
 <?php
      
      if(isset($_POST['submit'])){
        
        $product = new Product();
        $product->delete($_POST['check_list']);
          
        echo("<meta http-equiv='refresh' content='1'>"); 
      }
      
 ?>
  
      
  </body>
</html>